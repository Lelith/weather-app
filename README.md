# Weather App

This Application displays data from openweathermap

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

Important note: The application is not supported in Microsoft Edge in development mode. To see the application on these browsers you have to run the build and execute it on a static server.

### `yarn test`

Executes all tests. Use parameter -u to update outdated snapshot tests.

### `yarn build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />

### Run in production mode:

After executing `yarn build` you can install a static server with `yarn global add serve` and then execute `serve -s build`

### Future improvements

- Use a different iconset that the one from openweathermap which is available as SVG
- More optimization for responsiveness (on some edge cases the font is too big and overflows the screen)
- More accessible scrolling behaviour
- Support for older Internet Explorer Versions
- Internationalisation
- choice of City via Userinput
