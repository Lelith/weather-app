import React, { Component } from "react";
import loadWeatherData from "../../utils/fetchData";
import { setStorageItem, getStorageItem } from "../../utils/localStorage";
import { isEmptyObj } from "../../utils/helper";
import axios from "axios";
import moment from "moment";
import WeatherForecast from "../../components/weatherForecast";

import "./index.scss";
const CancelToken = axios.CancelToken;
const cancelSource = CancelToken.source();

export default class WeatherDisplay extends Component {
  constructor(props) {
    super(props);
    this.state = {
      weatherData: {},
      city: "Berlin",
      unit: "C"
    };
  }

  componentDidMount() {
    const savedState = getStorageItem("weatherApp") || {};
    if (
      isEmptyObj(savedState) ||
      !this.isSavedDataCurrent(savedState.weatherData)
    ) {
      this.getWeatherData();
    } else {
      this.setState(savedState);
    }
  }

  componentWillUnmount() {
    cancelSource.cancel();
  }

  isSavedDataCurrent(weatherData) {
    const firstEntry = moment.unix(weatherData.list[0].dt);
    return moment().isBefore(firstEntry, "hour");
  }

  getWeatherData() {
    const { city, unit } = this.state;
    const units = unit === "C" ? "metric" : "imperial";
    loadWeatherData(
      `http://api.openweathermap.org/data/2.5/forecast?q=${city}&&units=${units}&appid=db5a0b89330e4e2c3e5463cb9b4b10b3`,
      cancelSource
    ).then(resp => {
      this.setState(
        {
          weatherData: resp
        },
        () => {
          setStorageItem("weatherApp", this.state);
        }
      );
    });
  }

  render() {
    const { weatherData, unit } = this.state;
    return (
      <main className="weatherApp">
        {!isEmptyObj(weatherData) && (
          <WeatherForecast data={weatherData} unit={unit} />
        )}
      </main>
    );
  }
}
