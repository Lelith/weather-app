import React from "react";
import WeatherDisplay from "../";
import { shallow } from "enzyme";
import moment from "moment";
import { advanceTo } from "jest-date-mock";

const mockData = { list: [{ dt: 1487246400 }] }; // 16.02.2017 13:00

describe("isSavedDataCurrent", () => {
  it("retuns false if first entry is after current time", () => {
    advanceTo(moment("2017-02-16T20"));

    const wrapper = shallow(<WeatherDisplay />);
    const isToday = wrapper.instance().isSavedDataCurrent(mockData);
    expect(isToday).toBe(false);
  });
  it("retuns true if first entry is in the future", () => {
    advanceTo(moment("2017-02-16T08"));

    const wrapper = shallow(<WeatherDisplay />);
    const isToday = wrapper.instance().isSavedDataCurrent(mockData);
    expect(isToday).toBe(true);
  });
});
