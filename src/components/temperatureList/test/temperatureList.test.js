import React from "react";
import TemperatureList from "../";
import { shallow } from "enzyme";
import toJson from "enzyme-to-json";
import { mockData } from "./_mockData";

const callBackMock = jest.fn();

describe("Weather Symbol", () => {
  const temperatureListComp = shallow(
    <TemperatureList
      list={mockData}
      unit="C"
      currentSelected={1589641200}
      callbackFunc={callBackMock}
    />
  );

  it("renders three currentWeather Components", () => {
    expect(temperatureListComp.find("CurrentWeather").length).toBe(3);
  });

  it("highlights the current selected timeslot", () => {
    expect(
      temperatureListComp
        .find("CurrentWeather")
        .at(1)
        .prop("className")
    ).toBe("active");
  });

  it("calls the callback Function on click", () => {
    temperatureListComp
      .find("CurrentWeather")
      .at(2)
      .simulate("click");
    expect(callBackMock).toBeCalledWith(1589652000);
  });

  it("matches the snapshot", () => {
    expect(toJson(temperatureListComp)).toMatchSnapshot();
  });
});
