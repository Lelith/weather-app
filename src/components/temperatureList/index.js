import React from "react";
import PropTypes from "prop-types";
import CurrentWeather from "../currentWeather";
import classNames from "classnames";

import "./index.scss";

const TemperatureList = props => {
  const {
    list,
    unit,
    className,
    callbackFunc,
    currentSelected,
    ...other
  } = props;

  const temperatureListClasses = classNames("temperatureList", className);

  const isActive = currentDt => {
    return currentDt === currentSelected;
  };

  return (
    <section className={temperatureListClasses} {...other}>
      {list.map(tempEntry => {
        return (
          <CurrentWeather
            key={tempEntry.dt}
            dt={tempEntry.dt}
            temp={tempEntry.main.temp}
            symbol={{
              description: tempEntry.weather[0].description,
              icon: tempEntry.weather[0].icon
            }}
            unit={unit}
            className={isActive(tempEntry.dt) ? "active" : ""}
            onClick={() => callbackFunc(tempEntry.dt)}
            onKeyDown={() => callbackFunc(tempEntry.dt)}
            role="button"
            tabindex={0}
          />
        );
      })}
    </section>
  );
};

TemperatureList.propTypes = {
  list: PropTypes.array.isRequired,
  unit: PropTypes.string.isRequired,
  callbackFunc: PropTypes.func.isRequired,
  currentSelected: PropTypes.number.isRequired,
  className: PropTypes.string
};

TemperatureList.defaultProps = {
  className: ""
};

export default TemperatureList;
