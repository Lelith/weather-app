const mockData = [
  {"dt":1589630400,"main":{"temp":14.87,"feels_like":9.36,"temp_min":14.87,"temp_max":15.26,"pressure":1021,"sea_level":1021,"grnd_level":1016,"humidity":43,"temp_kf":-0.39},"weather":[{"id":802,"main":"Clouds","description":"scattered clouds","icon":"03d"}],"clouds":{"all":36},"wind":{"speed":5.58,"deg":262},"sys":{"pod":"d"},"dt_txt":"2020-05-16 12:00:00"},
  {"dt":1589641200,"main":{"temp":14.81,"feels_like":9.31,"temp_min":14.81,"temp_max":14.89,"pressure":1019,"sea_level":1019,"grnd_level":1014,"humidity":48,"temp_kf":-0.08},"weather":[{"id":803,"main":"Clouds","description":"broken clouds","icon":"04d"}],"clouds":{"all":81},"wind":{"speed":5.95,"deg":260},"sys":{"pod":"d"},"dt_txt":"2020-05-16 15:00:00"},
  {"dt":1589652000,"main":{"temp":13.52,"feels_like":9.61,"temp_min":13.45,"temp_max":13.52,"pressure":1019,"sea_level":1019,"grnd_level":1014,"humidity":57,"temp_kf":0.07},"weather":[{"id":803,"main":"Clouds","description":"broken clouds","icon":"04d"}],"clouds":{"all":84},"wind":{"speed":4.03,"deg":273},"sys":{"pod":"d"},"dt_txt":"2020-05-16 18:00:00"}
];

export {mockData};