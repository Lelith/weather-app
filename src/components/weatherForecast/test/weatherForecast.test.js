import React from "react";
import WeatherForecast from "../";
import { shallow } from "enzyme";
import toJson from "enzyme-to-json";
import { mockData } from "./_mockData";

describe("WeatherForecast", () => {
  const forecastComp = shallow(
    <WeatherForecast
      data={{ city: { name: "Berlin" }, list: mockData }}
      unit="C"
    />
  );

  forecastComp.update();

  it("sets the first entry for currentSelected timeslot and returns its data", () => {
    expect(forecastComp.state()).toEqual({
      currentSelected: 1589630400,
      currentSelectedData: mockData[0]
    });
  });

  it("fetches current selected data", () => {
    const data = forecastComp.instance().getCurrentSelectedData(1589652000);
    expect(data).toEqual(mockData[2]);
  });

  it("updates the state for new Selections", () => {
    forecastComp.instance().updateCurrentSelected(1589641200);
    expect(forecastComp.state()).toEqual({
      currentSelected: 1589641200,
      currentSelectedData: mockData[1]
    });
  });

  it("matches the snapshot", () => {
    expect(toJson(forecastComp)).toMatchSnapshot();
  });
});
