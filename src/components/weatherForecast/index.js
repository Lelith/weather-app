import React from "react";
import PropTypes from "prop-types";
import TemperatureList from "../temperatureList";
import WeatherOverview from "../weatherOverview";

class WeatherForecast extends React.Component {
  constructor(props) {
    super(props);
    const firstEntry = props.data.list[0].dt;
    this.state = {
      currentSelected: firstEntry,
      currentSelectedData: this.getCurrentSelectedData(firstEntry)
    };
    this.updateCurrentSelected = this.updateCurrentSelected.bind(this);
  }

  getCurrentSelectedData(currentSelected) {
    const { data } = this.props;
    const entries = data.list.filter(entry => entry.dt === currentSelected);
    return entries[0];
  }

  updateCurrentSelected(value) {
    this.setState({
      currentSelected: value,
      currentSelectedData: this.getCurrentSelectedData(value)
    });
  }

  render() {
    const { data, unit } = this.props;
    const city = data.city.name;
    const { currentSelected, currentSelectedData } = this.state;
    return (
      <>
        <WeatherOverview
          temp_max={currentSelectedData.main.temp_max}
          temp_min={currentSelectedData.main.temp_min}
          temp={currentSelectedData.main.temp}
          dt={currentSelectedData.dt}
          symbol={{
            description: currentSelectedData.weather[0].description,
            icon: currentSelectedData.weather[0].icon
          }}
          mainDescription={currentSelectedData.weather[0].main}
          city={city}
          unit={unit}
        />
        {data.list && data.list.length > 0 && (
          <TemperatureList
            list={data.list}
            unit={unit}
            currentSelected={currentSelected}
            callbackFunc={this.updateCurrentSelected}
          />
        )}
      </>
    );
  }
}

WeatherForecast.propTypes = {
  data: PropTypes.object.isRequired,
  unit: PropTypes.string.isRequired
};

export default WeatherForecast;
