import React from "react";
import WeatherSymbol from "../";
import { shallow } from "enzyme";
import toJson from "enzyme-to-json";

const symbol = {
  description: "light rain",
  icon: "10n"
};

describe("Weather Symbol", () => {
  const weatherImage = shallow(<WeatherSymbol symbol={symbol} />);
  it("contains a image description", () => {
    expect(weatherImage.prop("alt")).toBe("light rain");
  });
  it("shows the correct image ", () => {
    expect(weatherImage.prop("src")).toBe(
      "http://openweathermap.org/img/wn/10n@2x.png"
    );
  });
  it("matches the snapshot", () => {
    expect(toJson(weatherImage)).toMatchSnapshot();
  });
});
