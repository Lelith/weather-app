import React from "react";
import PropTypes from "prop-types";
import classNames from "classnames";

const WeatherSymbol = props => {
  const { symbol, className, ...other } = props;
  const { description, icon } = symbol;
  const symbolClasses = classNames("weatherSymbol", className);
  return (
    <img
      className={symbolClasses}
      alt={description}
      src={`http://openweathermap.org/img/wn/${icon}@2x.png`}
      {...other}
    />
  );
};

WeatherSymbol.propTypes = {
  symbol: PropTypes.shape({
    description: PropTypes.string,
    icon: PropTypes.string
  }).isRequired,
  className: PropTypes.string
};

WeatherSymbol.defaultProps = {
  className: ""
};

export default WeatherSymbol;
