import React from "react";
import PropTypes from "prop-types";
import WeatherSymbol from "../weatherSymbol";
import moment from "moment";
import "./index.scss";

const WeatherOverview = props => {
  const {
    unit,
    temp_max,
    temp_min,
    temp,
    city,
    dt,
    mainDescription,
    symbol
  } = props;

  const date = moment.unix(dt).utc();
  return (
    <section className="overview">
      <WeatherSymbol symbol={symbol} />
      <summary className="overview__summary">
        <div className="overview__summary__headline">
          <span className="overview__description">{mainDescription}</span>
          <span className="overview__temperature--minmax">
            {`${temp_max}°${unit}`} / {`${temp_min}°${unit}`}{" "}
          </span>
        </div>
        <span className="overview__temperature">{`${temp}°${unit}`}</span>
      </summary>
      <summary className="overview__summary">
        <span className="overview__summary__headline">{city}</span>
        <div className="overview__weekdate">
          <span className="overview__weekday">
            {moment(date).format("dddd")}
          </span>
          <span className="overview__date">
            {moment(date).format("DD. MMMM")}
          </span>
        </div>
      </summary>
    </section>
  );
};

WeatherOverview.propTypes = {
  temp_max: PropTypes.number.isRequired,
  temp_min: PropTypes.number.isRequired,
  temp: PropTypes.number.isRequired,
  city: PropTypes.string.isRequired,
  dt: PropTypes.number.isRequired,
  symbol: PropTypes.object.isRequired,
  mainDescription: PropTypes.string.isRequired,
  unit: PropTypes.string
};

export default WeatherOverview;
