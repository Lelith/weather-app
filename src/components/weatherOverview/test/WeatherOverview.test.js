import React from 'react';
import WeatherOverview from '../';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
const symbol = {
  "description": "light rain",
  "icon": "10n"
};

describe('Weather Symbol', () => {
  const weatherComp = shallow(
    <WeatherOverview
    city="Berlin"
    temp_max={15}
    temp_min={14}
    temp={14.01}
    dt={1589641200}
    symbol={symbol}
    unit="C"
    mainDescription="Clear"
    />
  );

  it('matches the snapshot', () => {
    expect(toJson(weatherComp)).toMatchSnapshot();
  });
});
