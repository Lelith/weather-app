import React from "react";
import PropTypes from "prop-types";
import WeatherSymbol from "../weatherSymbol";
import moment from "moment";
import classNames from "classnames";
import "./index.scss";
const CurrentWeather = props => {
  const { dt, temp, unit, symbol, className, ...other } = props;

  const currentWeatherClasses = classNames("currentWeather", className);

  return (
    <section className={currentWeatherClasses} {...other}>
      <time className="currentWeather__hour">
        {moment(moment.unix(dt)).format("HH:00")}
      </time>
      <WeatherSymbol symbol={symbol} />
      <span className="currentWeather__temperature">{`${temp}°${unit}`}</span>
    </section>
  );
};

CurrentWeather.propTypes = {
  dt: PropTypes.number.isRequired,
  temp: PropTypes.number.isRequired,
  unit: PropTypes.string.isRequired,
  symbol: PropTypes.object.isRequired,
  className: PropTypes.string
};

CurrentWeather.defaultProps = {
  className: ""
};

export default CurrentWeather;
