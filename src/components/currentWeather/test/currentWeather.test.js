import React from "react";
import CurrentWeather from "../";
import { shallow } from "enzyme";
import toJson from "enzyme-to-json";
const symbol = {
  description: "light rain",
  icon: "10n"
};

describe("Weather Symbol", () => {
  const currentWeatherComp = shallow(
    <CurrentWeather temp={14.01} dt={1589641200} symbol={symbol} unit="C" />
  );

  it("matches the snapshot", () => {
    expect(toJson(currentWeatherComp)).toMatchSnapshot();
  });
});
