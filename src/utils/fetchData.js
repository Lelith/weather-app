import axios from "axios";

/**
 * loadWeatherData
 * @param {string} Url where to fetch the json data
 * @param {Axios Token} cancelSource axios cancel token
 * @return {Promise}
 */
export default function loadWeatherData(url, cancelSource) {
  return axios
    .get(url, { cancelToken: cancelSource.token })
    .then(response => {
      return response.data;
    })
    .catch(error => {
      const errMessage = `weather data ${url}`;
      console.error(error, errMessage);
      return "loadError";
    });
}
