/**
 * getStorageItem
 * @param  {string} Item to get from localStorage
 * @return {object}
 */

export const getStorageItem = item => {
  const result = localStorage.getItem(item);
  return JSON.parse(result);
};

/**
 * setStorageItem
 * @param  {string} Item to store to localStorage
 * @param  {Object} value to store to localStorage
 * @return {object}
 */

export const setStorageItem = (item, value) => {
  const data = JSON.stringify(value);
  localStorage.setItem(item, data);
};

/**
 * delStorageItem.
 * @param  {string} item to delete to localStorage item
 */
export const delStorageItem = item => {
  localStorage.removeItem(item);
};

/**
 * clearStorage
 */
export const clearStorage = () => {
  localStorage.clear();
};
