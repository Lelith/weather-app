/**
 * isEmptyObj
 * @param  {Object} obj object which should be checked
 * @return {object}
 */

export const isEmptyObj = obj => {
  for (var key in obj) {
    if (obj.hasOwnProperty(key)) return false;
  }
  return true;
};
