import React from "react";

import WeatherDisplay from "./container/weatherDisplay";

function App() {
  return (
    <div className="app">
      <WeatherDisplay />
    </div>
  );
}

export default App;
